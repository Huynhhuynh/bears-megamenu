<?php
/***
 * @package Bears Megamenu
 * Template admin
 *
 * @since 1.0.0
 */

?>
<div id="BMM_App" class="bmm-container">
    <bmm-modal :is-open="Edit.modal_display">
        <!-- <pre>{{ $store.getters.getMenuCurrent }}</pre> -->
        <template slot="heading">{{ MenuCurrentTitle }}</template>
        <div class="setting-field-wrap">
            <bmm-load-option-ui :reload="Edit.modal_display" :menu-item-data="EditMenuCurrent"></bmm-load-option-ui>
        </div>
        <div slot="footer" class="actions">
            <el-button type="success" :loading="$store.getters.getButtonSaveStatus" @click="SaveMenuItemData"><?php _e('Save', 'bears-megamenu') ?></el-button>
        </div>
    </bmm-modal>
</div>

<!-- BMM Modal -->
<script type="x-template" id="bmm-modal-template">
    <div :class="classes_wrap">
        <div class="bmm--modal-inner">
            <a href="javascript:" class="bmm--close" @click="close_modal"><span class="dashicons dashicons-no-alt"></span> <?php _e('Close', 'bears-megamenu'); ?></a>
            <div class="bmm--modal-heading">
                <slot name="heading"></slot>
            </div>

            <div class="bmm--modal-body">
                <slot></slot>
            </div>

            <div class="bmm--modal-footer">
                <slot name="footer"></slot>
            </div>
        </div>
    </div>
</script>

<script type="x-template" id="bmm-option-group-lvl-0">
    <div class="bmm--option-group-wrap">
        <el-form ref="form" :model="data" label-width="220px" label-position="left">
            <el-form-item label="<?php _e('Menu Icon', 'bears-megamenu'); ?>">
                <el-switch on-text="" off-text="" on-value="yes"  off-value="no" v-model="data.icon"></el-switch>
                <div class="bmm__tip">On/Off menu icon</div>
            </el-form-item>
            <el-form-item v-show="data.icon == 'yes'" label="<?php _e('Icon Class', 'bears-megamenu'); ?>">
                <el-input v-model="data.icon_class"></el-input>
                <div class="bmm__tip">
                    Use
                    <a href="http://ionicons.com/" target="_blank">ionicons</a>,
                    <a href="https://developer.wordpress.org/resource/dashicons/#media-default">dashicons</a> (Ex: 'dashicons dashicons-admin-site'),
                    <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank">fontawesome</a> (Ex: 'fa fa-globe') classes.
                </div>
            </el-form-item>

            <hr />

            <el-form-item label="<?php _e('Menu Badge', 'bears-megamenu'); ?>">
                <el-switch on-text="" off-text="" on-value="yes"  off-value="no" v-model="data.badge"></el-switch>
                <div class="bmm__tip">On/Off menu badge</div>
            </el-form-item>
            <div v-show="data.badge == 'yes'">
                <el-form-item  label="<?php _e('Badge Text', 'bears-megamenu'); ?>">
                    <el-input v-model="data.badge_text"></el-input>
                    <div class="bmm__tip">Enter menu badge text</div>
                </el-form-item>
                <el-form-item  label="<?php _e('Badge Color / Background', 'bears-megamenu'); ?>">
                    <el-color-picker v-model="data.badge_color"></el-color-picker>
                    <el-color-picker v-model="data.badge_background"></el-color-picker>
                </el-form-item>
            </div>

            <hr />

            <el-form-item label="<?php _e('Mega Menu', 'bears-megamenu'); ?>">
                <el-switch on-text="" off-text="" on-value="yes"  off-value="no" v-model="data.megamenu"></el-switch>
                <div class="bmm__tip">On/Off megamenu</div>
            </el-form-item>
            <div v-show="data.megamenu == 'yes'">
                <el-form-item  label="<?php _e('Mega Menu Alignment', 'bears-megamenu'); ?>">
                    <!-- <el-switch on-text="" off-text="" on-value="yes"  off-value="no" v-model="data.megamenu_center_mode"></el-switch> -->
                    <el-radio-group v-model="data.megamenu_alignment">
                        <el-radio-button label="left"><?php _e('Left', 'bears-megamenu') ?></el-radio-button>
                        <el-radio-button label="center"><?php _e('Center', 'bears-megamenu') ?></el-radio-button>
                        <el-radio-button label="right"><?php _e('Right', 'bears-megamenu') ?></el-radio-button>
                    </el-radio-group>
                    <div class="bmm__tip">On/Off megamenu alignment with parent menu item</div>
                </el-form-item>
                <!-- <el-form-item  label="<?php _e('Mega Menu Background Color', 'bears-megamenu'); ?>">
                    <el-color-picker v-model="data.megamenu_background_color"></el-color-picker>
                    <div class="bmm__tip">Field megamenu background color (default: #FFF)</div>
                </el-form-item>
                <el-form-item  label="<?php _e('Mega Menu Background Image', 'bears-megamenu'); ?>">
                    <el-input v-model="data.megamenu_background_image"></el-input>
                    <div class="bmm__tip">Field megamenu background image url (<a href="<?php echo esc_attr( get_admin_url() . 'upload.php' ); ?>" target="_blank">Open media</a>)</div>
                </el-form-item> -->
            </div>
            <div v-show="data.megamenu == 'no'">
              <el-form-item label="<?php _e('Disable minWidth Submenu', 'bears-megamenu'); ?>">
                  <el-switch on-text="" off-text="" on-value="yes"  off-value="no" v-model="data.minwidth"></el-switch>
                  <div class="bmm__tip">On/Off minWidth of submenu</div>
              </el-form-item>
            </div>

        <el-form>
    </div>
</script>

<script type="x-template" id="bmm-option-group-lvl-1">
    <div class="bmm--option-group-wrap">
    <el-form ref="form" :model="data" label-width="220px" label-position="left">
            <el-form-item label="<?php _e('Menu Icon', 'bears-megamenu'); ?>">
                <el-switch on-text="" off-text="" on-value="yes"  off-value="no" v-model="data.icon"></el-switch>
                <div class="bmm__tip">On/Off menu icon</div>
            </el-form-item>
            <el-form-item v-show="data.icon == 'yes'" label="<?php _e('Icon Class', 'bears-megamenu'); ?>">
                <el-input v-model="data.icon_class"></el-input>
                <div class="bmm__tip">
                    Use
                    <a href="http://ionicons.com/" target="_blank">ionicons</a>,
                    <a href="https://developer.wordpress.org/resource/dashicons/#media-default">dashicons</a> (Ex: 'dashicons dashicons-admin-site'),
                    <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank">fontawesome</a> (Ex: 'fa fa-globe') classes.
                </div>
            </el-form-item>

            <hr />

            <el-form-item label="<?php _e('Menu Badge', 'bears-megamenu'); ?>">
                <el-switch on-text="" off-text="" on-value="yes"  off-value="no" v-model="data.badge"></el-switch>
                <div class="bmm__tip">On/Off menu badge</div>
            </el-form-item>
            <div v-show="data.badge == 'yes'">
                <el-form-item  label="<?php _e('Badge Text', 'bears-megamenu'); ?>">
                    <el-input v-model="data.badge_text"></el-input>
                    <div class="bmm__tip">Enter menu badge text</div>
                </el-form-item>
                <el-form-item  label="<?php _e('Badge Color / Background', 'bears-megamenu'); ?>">
                    <el-color-picker v-model="data.badge_color"></el-color-picker>
                    <el-color-picker v-model="data.badge_background"></el-color-picker>
                </el-form-item>
            </div>

            <hr />

            <el-form-item label="<?php _e('Custom Menu Item Width', 'bears-megamenu'); ?>">
                <el-switch on-text="" off-text="" on-value="yes"  off-value="no" v-model="data.custom_menu_item_width"></el-switch>
                <div class="bmm__tip">On/Off custom menu item width</div>
            </el-form-item>
            <el-form-item v-show="data.custom_menu_item_width == 'yes'" label="<?php _e('Menu Item Width', 'bears-megamenu'); ?>">
                <el-input v-model="data.menu_item_width"></el-input>
                <div class="bmm__tip">
                    Enter custom with menu item.
                </div>
            </el-form-item>

            <hr />

            <el-form-item label="<?php _e('Menu Item Heading Style', 'bears-megamenu'); ?>">
                <el-switch on-text="" off-text="" on-value="yes"  off-value="no" v-model="data.menu_item_heading_style"></el-switch>
                <div class="bmm__tip">On/Off menu item heading style</div>
            </el-form-item>

        <el-form>
    </div>
</script>

<script type="x-template" id="bmm-option-group-lvl-etc">
    <div class="bmm--option-group-wrap">
    <el-form ref="form" :model="data" label-width="220px" label-position="left">
            <el-form-item label="<?php _e('Menu Icon', 'bears-megamenu'); ?>">
                <el-switch on-text="" off-text="" on-value="yes"  off-value="no" v-model="data.icon"></el-switch>
                <div class="bmm__tip">On/Off menu icon</div>
            </el-form-item>
            <el-form-item v-show="data.icon == 'yes'" label="<?php _e('Icon Class', 'bears-megamenu'); ?>">
                <el-input v-model="data.icon_class"></el-input>
                <div class="bmm__tip">
                    Use
                    <a href="http://ionicons.com/" target="_blank">ionicons</a>,
                    <a href="https://developer.wordpress.org/resource/dashicons/#media-default">dashicons</a> (Ex: 'dashicons dashicons-admin-site'),
                    <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank">fontawesome</a> (Ex: 'fa fa-globe') classes.
                </div>
            </el-form-item>

            <hr />

            <el-form-item label="<?php _e('Menu Badge', 'bears-megamenu'); ?>">
                <el-switch on-text="" off-text="" on-value="yes"  off-value="no" v-model="data.badge"></el-switch>
                <div class="bmm__tip">On/Off menu badge</div>
            </el-form-item>
            <div v-show="data.badge == 'yes'">
                <el-form-item  label="<?php _e('Badge Text', 'bears-megamenu'); ?>">
                    <el-input v-model="data.badge_text"></el-input>
                    <div class="bmm__tip">Enter menu badge text</div>
                </el-form-item>
                <el-form-item  label="<?php _e('Badge Color / Background', 'bears-megamenu'); ?>">
                    <el-color-picker v-model="data.badge_color"></el-color-picker>
                    <el-color-picker v-model="data.badge_background"></el-color-picker>
                </el-form-item>
            </div>

            <hr />

            <el-form-item label="<?php _e('Menu Item Heading Style', 'bears-megamenu'); ?>">
                <el-switch on-text="" off-text="" on-value="yes"  off-value="no" v-model="data.menu_item_heading_style"></el-switch>
                <div class="bmm__tip">On/Off menu item heading style</div>
            </el-form-item>

        <el-form>
    </div>
</script>
