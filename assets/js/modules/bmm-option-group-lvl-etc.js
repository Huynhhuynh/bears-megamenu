export default {
    props: ['data'],
    data () {
        return {}
    },
    created(el) {
        
    },
    template: '#bmm-option-group-lvl-etc',
    watch: {
        data: {
            handler(data) {
                this.$store.commit('setMenuCurrentData', data);
            },
            deep: true,
        }
    },
    computed: {
       
    },
    methods: {
        
    },
}