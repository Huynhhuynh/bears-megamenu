var $ = jQuery.noConflict();

var BmmLoadOptionsUi = {
    props: ['reload', 'MenuItemData'],
    template: `
    <div>
        <component v-if="!loading" :is="currentSettingComponent" :data="data"></component>
    </div>`,
    data() {
        return {
            loading: true,
            data: {},
        }
    },
    created(el) {

    },
    watch: {
        reload(data) {
            if( data == true ) this.loadOpitons();
        }
    },
    computed: {
        currentSettingComponent() {
            var MenuDepthSupport = $.inArray( parseInt(this.MenuItemData.menu_depth), bmm_admin_localize_script.menu_depth_support );
            var menuItemLvl = (MenuDepthSupport >= 0) ? this.MenuItemData.menu_depth : 'etc';

            return ['bmm-option-group-lvl', menuItemLvl].join('-');
        },
    },
    methods: {
        loadOpitons() {
            var self = this;
            this.loading = true;

            $.ajax({
                type: 'POST',
                url: bmm_admin_localize_script.ajax_url,
                data: {
                    action: 'BMM_Ajax_Handle',
                    handle: 'BMM_Load_Options_Menu_Item',
                    params: this.MenuItemData
                },
                success(res) {
                    // console.log(res);
                    self.loading = false;

                    if(res.success == true) {
                        console.log(res.data);
                        Vue.set(self, 'data', res.data);
                        self.$store.commit('setMenuCurrentData', res.data);
                    } else {
                        // fail
                    }
                },
                error(e) {
                    console.log(e);
                }
            })
        }
    }
}

export default BmmLoadOptionsUi;