const BMM_Modal = {
    props: ['IsOpen', 'MenuItemData'],
    data () {
        return {}
    },
    template: '#bmm-modal-template',
    created(el) {

    },
    computed: {
        classes_wrap() {
            var is_open = this.IsOpen ? 'is-open' : 'is-close';
            return ['bmm--modal-container', is_open].join(' ');
        }
    },
    methods: {
        close_modal() {
            this.$store.commit('setModalDisplay', false);
        }
    },
}

export default BMM_Modal;