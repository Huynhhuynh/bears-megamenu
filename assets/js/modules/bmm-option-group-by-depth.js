export default function(templateID) {
    return {
        props: ['data'],
        data () {
            return {}
        },
        created(el) {
            
        },
        // template: '#option-group-lvl-0',
        template: templateID,
        watch: {
            data: {
                handler(data) {
                    this.$store.commit('setMenuCurrentData', data);
                },
                deep: true,
            }
        },
        computed: {
           
        },
        methods: {
            
        },
    }
}