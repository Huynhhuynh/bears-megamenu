/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./backend.bears-megamenu.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./backend.bears-megamenu.js":
/*!***********************************!*\
  !*** ./backend.bears-megamenu.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_modal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/modal */ "./modules/modal.js");
/* harmony import */ var _modules_bmm_load_option_ui_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/bmm-load-option-ui.js */ "./modules/bmm-load-option-ui.js");
/* harmony import */ var _modules_bmm_option_group_by_depth_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/bmm-option-group-by-depth.js */ "./modules/bmm-option-group-by-depth.js");
/* harmony import */ var _modules_bmm_option_group_lvl_etc_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/bmm-option-group-lvl-etc.js */ "./modules/bmm-option-group-lvl-etc.js");
/**
 * @package Bears Megamenu
 * Javascript
 * 
 * @since 1.0.0
 */






(function(w, $) {
    'use strict';

    ELEMENT.locale(ELEMENT.lang.en);

    const $MBB_MenuToEdit =  $("#menu-to-edit");
    const BMM_Store = new Vuex.Store({
        state: {
            Edit: {
                modal_display: false,
                modal_loading: true,
                button_save_is_loading: false,
            },
            CurrentMenuEdit: {
                menu_id: 0,
                menu_title: '',
                menu_depth: 0,
                data: {},
            },
        },
        mutations: {
            setMenuEdit (state, data) {
                // state.CurrentMenuEdit = data;
                $.extend(state.CurrentMenuEdit, data);
            },
            setModalDisplay(state, st) {
                state.Edit.modal_display = st;
            },
            setButtonSaveStatus(state, st) {
                state.Edit.button_save_is_loading = st;
            },
            setMenuCurrentData(state, data) {
                state.CurrentMenuEdit.data = data;
            },
        },
        getters: {
            getButtonSaveStatus(state) {
                return state.Edit.button_save_is_loading;
            },
            getMenuCurrent(state) {
                return state.CurrentMenuEdit;
            }
        },
    })

    /** 
     * @since 1.0.0
     * Add event tirgger 
     */
    var BMM_AddEventMenuToEdit = function() {
        $MBB_MenuToEdit.on({
            '_add_button_setting.bears_megamenu' () {
                $(this).find('li.menu-item').each(function() {
                    if($(this).data('megamenu-has-button') == true) return;

                    var button = $("<span>").addClass("bmm_megamenu_launch").html(`${bmm_admin_localize_script.language.TEXT_ADVANCED_SETTINGS}`);
                    var menu_item = $(this);
                    var menu_id = $("input#menu").val();

                    button.on('click', function() {
                        var _menu_item = $(this).closest('li.menu-item');
                        var title = _menu_item.find(".menu-item-title").text();
                        var id = parseInt(_menu_item.attr("id").match(/[0-9]+/)[0], 10);
                        var depth = _menu_item.attr("class").match(/\menu-item-depth-(\d+)\b/)[1];
                        
                        BMM_Store.commit('setMenuEdit', {
                            menu_id: id,
                            menu_title: title,
                            menu_depth: depth,
                        });

                        BMM_Store.commit('setModalDisplay', true);
                    })

                    $(".item-title", menu_item).append(button);

                    menu_item.data("megamenu-has-button", true);
                })
            } 
        })
    }
    BMM_AddEventMenuToEdit();

    /** 
     * @since 1.0.0
     * Add button megamenu setting for menu item 
     */
    var BMM_AddButtonAdvancedSetting = function() {
        $MBB_MenuToEdit.trigger('_add_button_setting')
    }

    // DOM Ready
    $(function() {
        // add button advanced settings
        BMM_AddButtonAdvancedSetting();

        // trigger event add new menu item
        $(document).on('menu-item-added', BMM_AddButtonAdvancedSetting);

        if($('#BMM_App').length > 0) {

            // load menu depth support
            bmm_admin_localize_script.menu_depth_support.forEach(function(item) {
                var templateID = ['#bmm-option-group-lvl', item].join('-');
                if( $(templateID).length > 0 ) {
                    Vue.component( ['bmm-option-group-lvl', item].join('-'), Object(_modules_bmm_option_group_by_depth_js__WEBPACK_IMPORTED_MODULE_2__["default"])( templateID ) );
                }
            })
            // Vue.component('bmm-option-group-lvl-0', BMM_Option_Group_Lvl0);
            Vue.component('bmm-option-group-lvl-etc', _modules_bmm_option_group_lvl_etc_js__WEBPACK_IMPORTED_MODULE_3__["default"]);

            w.BMM_APP = new Vue({
                el: '#BMM_App',
                store: BMM_Store,
                data () {
                    return {
                        
                    };
                },
                components: {
                    'bmm-modal': _modules_modal__WEBPACK_IMPORTED_MODULE_0__["default"],
                    'bmm-load-option-ui': _modules_bmm_load_option_ui_js__WEBPACK_IMPORTED_MODULE_1__["default"],
                    // 'option-group-lvl-0': BMM_Option_Group_Lvl0,
                },
                created (el) {
    
                },
                computed: {
                    Edit () {
                        return this.$store.state.Edit;
                    },
                    EditMenuCurrent () {
                        return this.$store.state.CurrentMenuEdit;
                    },
                    MenuCurrentID () {
                        return this.EditMenuCurrent.menu_id;
                    },
                    MenuCurrentTitle () {
                        return this.EditMenuCurrent.menu_title;
                    },
                    // ModalDisplay () {
                    //     return ( this.MenuCurrentID && this.MenuCurrentID != 0 ) ? true : false;
                    // }
                },
                methods: {
                    SaveMenuItemData() {
                        var self = this;

                        this.$store.commit('setButtonSaveStatus', true);
                        $.ajax({
                            type: 'POST',
                            url: bmm_admin_localize_script.ajax_url,
                            data: {
                                action: 'BMM_Ajax_Handle',
                                handle: 'BMM_Save_Menu_Item_Data',
                                params: self.$store.getters.getMenuCurrent,
                            },
                            success(res) {
                                self.$store.commit('setButtonSaveStatus', false);
                                console.log(res);
                            },
                            error(e) {
                                console.log(e)
                            }
                        })
                    }
                }
            })
        }
    })
})(window, jQuery)

/***/ }),

/***/ "./modules/bmm-load-option-ui.js":
/*!***************************************!*\
  !*** ./modules/bmm-load-option-ui.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var $ = jQuery.noConflict();

var BmmLoadOptionsUi = {
    props: ['reload', 'MenuItemData'],
    template: `
    <div>
        <component v-if="!loading" :is="currentSettingComponent" :data="data"></component>
    </div>`,
    data() {
        return {
            loading: true,
            data: {},
        }
    },
    created(el) {

    },
    watch: {
        reload(data) {
            if( data == true ) this.loadOpitons();
        }
    },
    computed: {
        currentSettingComponent() {
            var MenuDepthSupport = $.inArray( parseInt(this.MenuItemData.menu_depth), bmm_admin_localize_script.menu_depth_support );
            var menuItemLvl = (MenuDepthSupport >= 0) ? this.MenuItemData.menu_depth : 'etc';

            return ['bmm-option-group-lvl', menuItemLvl].join('-');
        },
    },
    methods: {
        loadOpitons() {
            var self = this;
            this.loading = true;

            $.ajax({
                type: 'POST',
                url: bmm_admin_localize_script.ajax_url,
                data: {
                    action: 'BMM_Ajax_Handle',
                    handle: 'BMM_Load_Options_Menu_Item',
                    params: this.MenuItemData
                },
                success(res) {
                    // console.log(res);
                    self.loading = false;

                    if(res.success == true) {
                        console.log(res.data);
                        Vue.set(self, 'data', res.data);
                        self.$store.commit('setMenuCurrentData', res.data);
                    } else {
                        // fail
                    }
                },
                error(e) {
                    console.log(e);
                }
            })
        }
    }
}

/* harmony default export */ __webpack_exports__["default"] = (BmmLoadOptionsUi);

/***/ }),

/***/ "./modules/bmm-option-group-by-depth.js":
/*!**********************************************!*\
  !*** ./modules/bmm-option-group-by-depth.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function(templateID) {
    return {
        props: ['data'],
        data () {
            return {}
        },
        created(el) {
            
        },
        // template: '#option-group-lvl-0',
        template: templateID,
        watch: {
            data: {
                handler(data) {
                    this.$store.commit('setMenuCurrentData', data);
                },
                deep: true,
            }
        },
        computed: {
           
        },
        methods: {
            
        },
    }
});

/***/ }),

/***/ "./modules/bmm-option-group-lvl-etc.js":
/*!*********************************************!*\
  !*** ./modules/bmm-option-group-lvl-etc.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['data'],
    data () {
        return {}
    },
    created(el) {
        
    },
    template: '#bmm-option-group-lvl-etc',
    watch: {
        data: {
            handler(data) {
                this.$store.commit('setMenuCurrentData', data);
            },
            deep: true,
        }
    },
    computed: {
       
    },
    methods: {
        
    },
});

/***/ }),

/***/ "./modules/modal.js":
/*!**************************!*\
  !*** ./modules/modal.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const BMM_Modal = {
    props: ['IsOpen', 'MenuItemData'],
    data () {
        return {}
    },
    template: '#bmm-modal-template',
    created(el) {

    },
    computed: {
        classes_wrap() {
            var is_open = this.IsOpen ? 'is-open' : 'is-close';
            return ['bmm--modal-container', is_open].join(' ');
        }
    },
    methods: {
        close_modal() {
            this.$store.commit('setModalDisplay', false);
        }
    },
}

/* harmony default export */ __webpack_exports__["default"] = (BMM_Modal);

/***/ })

/******/ });
//# sourceMappingURL=backend-bears-megamenu.bundle.js.map