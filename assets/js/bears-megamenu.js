/**
 * @package Bears Megamenu
 * Bears WordPress Megamenu Script
 * 
 * @since 1.0.0
 */

(function(w, $) {
    'use strict';

    // DOM Loaded
    $(function() {

    })

    // Browser Loaded
    $(w).load(function() {

    })
})(window, jQuery)