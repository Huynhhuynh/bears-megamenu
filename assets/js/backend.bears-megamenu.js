/**
 * @package Bears Megamenu
 * Javascript
 * 
 * @since 1.0.0
 */

import BMM_Modal from './modules/modal';
import BMM_Load_Option_Ui from './modules/bmm-load-option-ui.js';
import BMM_Option_Group_By_Depth from './modules/bmm-option-group-by-depth.js';
import BMM_Option_Group_LvlEtc from './modules/bmm-option-group-lvl-etc.js';

(function(w, $) {
    'use strict';

    ELEMENT.locale(ELEMENT.lang.en);

    const $MBB_MenuToEdit =  $("#menu-to-edit");
    const BMM_Store = new Vuex.Store({
        state: {
            Edit: {
                modal_display: false,
                modal_loading: true,
                button_save_is_loading: false,
            },
            CurrentMenuEdit: {
                menu_id: 0,
                menu_title: '',
                menu_depth: 0,
                data: {},
            },
        },
        mutations: {
            setMenuEdit (state, data) {
                // state.CurrentMenuEdit = data;
                $.extend(state.CurrentMenuEdit, data);
            },
            setModalDisplay(state, st) {
                state.Edit.modal_display = st;
            },
            setButtonSaveStatus(state, st) {
                state.Edit.button_save_is_loading = st;
            },
            setMenuCurrentData(state, data) {
                state.CurrentMenuEdit.data = data;
            },
        },
        getters: {
            getButtonSaveStatus(state) {
                return state.Edit.button_save_is_loading;
            },
            getMenuCurrent(state) {
                return state.CurrentMenuEdit;
            }
        },
    })

    /** 
     * @since 1.0.0
     * Add event tirgger 
     */
    var BMM_AddEventMenuToEdit = function() {
        $MBB_MenuToEdit.on({
            '_add_button_setting.bears_megamenu' () {
                $(this).find('li.menu-item').each(function() {
                    if($(this).data('megamenu-has-button') == true) return;

                    var button = $("<span>").addClass("bmm_megamenu_launch").html(`${bmm_admin_localize_script.language.TEXT_ADVANCED_SETTINGS}`);
                    var menu_item = $(this);
                    var menu_id = $("input#menu").val();

                    button.on('click', function() {
                        var _menu_item = $(this).closest('li.menu-item');
                        var title = _menu_item.find(".menu-item-title").text();
                        var id = parseInt(_menu_item.attr("id").match(/[0-9]+/)[0], 10);
                        var depth = _menu_item.attr("class").match(/\menu-item-depth-(\d+)\b/)[1];
                        
                        BMM_Store.commit('setMenuEdit', {
                            menu_id: id,
                            menu_title: title,
                            menu_depth: depth,
                        });

                        BMM_Store.commit('setModalDisplay', true);
                    })

                    $(".item-title", menu_item).append(button);

                    menu_item.data("megamenu-has-button", true);
                })
            } 
        })
    }
    BMM_AddEventMenuToEdit();

    /** 
     * @since 1.0.0
     * Add button megamenu setting for menu item 
     */
    var BMM_AddButtonAdvancedSetting = function() {
        $MBB_MenuToEdit.trigger('_add_button_setting')
    }

    // DOM Ready
    $(function() {
        // add button advanced settings
        BMM_AddButtonAdvancedSetting();

        // trigger event add new menu item
        $(document).on('menu-item-added', BMM_AddButtonAdvancedSetting);

        if($('#BMM_App').length > 0) {

            // load menu depth support
            bmm_admin_localize_script.menu_depth_support.forEach(function(item) {
                var templateID = ['#bmm-option-group-lvl', item].join('-');
                if( $(templateID).length > 0 ) {
                    Vue.component( ['bmm-option-group-lvl', item].join('-'), BMM_Option_Group_By_Depth( templateID ) );
                }
            })
            // Vue.component('bmm-option-group-lvl-0', BMM_Option_Group_Lvl0);
            Vue.component('bmm-option-group-lvl-etc', BMM_Option_Group_LvlEtc);

            w.BMM_APP = new Vue({
                el: '#BMM_App',
                store: BMM_Store,
                data () {
                    return {
                        
                    };
                },
                components: {
                    'bmm-modal': BMM_Modal,
                    'bmm-load-option-ui': BMM_Load_Option_Ui,
                    // 'option-group-lvl-0': BMM_Option_Group_Lvl0,
                },
                created (el) {
    
                },
                computed: {
                    Edit () {
                        return this.$store.state.Edit;
                    },
                    EditMenuCurrent () {
                        return this.$store.state.CurrentMenuEdit;
                    },
                    MenuCurrentID () {
                        return this.EditMenuCurrent.menu_id;
                    },
                    MenuCurrentTitle () {
                        return this.EditMenuCurrent.menu_title;
                    },
                    // ModalDisplay () {
                    //     return ( this.MenuCurrentID && this.MenuCurrentID != 0 ) ? true : false;
                    // }
                },
                methods: {
                    SaveMenuItemData() {
                        var self = this;

                        this.$store.commit('setButtonSaveStatus', true);
                        $.ajax({
                            type: 'POST',
                            url: bmm_admin_localize_script.ajax_url,
                            data: {
                                action: 'BMM_Ajax_Handle',
                                handle: 'BMM_Save_Menu_Item_Data',
                                params: self.$store.getters.getMenuCurrent,
                            },
                            success(res) {
                                self.$store.commit('setButtonSaveStatus', false);
                                console.log(res);
                            },
                            error(e) {
                                console.log(e)
                            }
                        })
                    }
                }
            })
        }
    })
})(window, jQuery)