<?php 
return array(
    // general options
    'icon' => 'no',
    'icon_class' => 'ion-earth',
    'badge' => 'no',
    'badge_text' => __('Hot', 'bears-megamenu'),
    'badge_color' => '#fff',
    'badge_background' => '#f56c6c',
    // 'menu_item_heading_style' => 'no', // explode depth menu lvl 0
    'custom_class' => '',
);