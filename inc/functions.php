<?php
/**
 * @package Bears Megamenu
 * @since 1.0.0
 * functions
 */

if(! function_exists('BMM_Render_Scss')) {
    /**
     * scss compile
     * @since 1.0.0
     *
     * @param $input_file   - scss main file
     * @param $output_file  - css output file
     * @param $import_path  - http://leafo.net/scssphp/docs/#import_paths
     */
    function BMM_Render_Scss($input_file = null, $output_file = null, $import_path = null) {

        /* work on BMM_MODE true */
        if(BMM_MODE != true || class_exists('Leafo\ScssPhp\Compiler') != true) return;

        global $wp_filesystem;
        if( empty( $wp_filesystem ) ) {
            require_once(ABSPATH . 'wp-admin/includes/file.php');
            WP_Filesystem();
        }

        $scss = new Leafo\ScssPhp\Compiler();

        $scss->setVariables(array(

        ));

        $scss->setFormatter("Leafo\ScssPhp\Formatter\Compressed");
        $scss->setImportPaths($import_path);

        $chmod_dir = ( 0755 & ~ umask() );
		if ( defined( 'FS_CHMOD_DIR' ) ) {
	  	    $chmod_dir = FS_CHMOD_DIR;
		}

        $scss_content = $scss->compile(apply_filters('bmm_scss_content', $wp_filesystem->get_contents( $input_file )));
        $wp_filesystem->put_contents( $output_file, $scss_content, $chmod_dir );
    }
}

if(! function_exists('BMM_Template')) {
    /**
     * @since 1.0.0
     */
    function BMM_Template() {
        include BMM_DIR_PATH . '/templates/admin/main.php';
    }
}

if(! function_exists('BMM_Default_Options')) {
    /**
     * @since 1.0.0
     *
     * @param {int} $depth
     * @return {array}
     */
    function BMM_Default_Options($depth = 0) {
        $options_default = include __DIR__ . '/opts-default.php';
        return apply_filters( 'BMM_options_default_filter', $options_default, $depth );
    }
}

if(! function_exists('BMM_add_megamenu_optons_add_opt_heading_style')) {
    function BMM_add_megamenu_optons_add_opt_heading_style($opts = array(), $depth = 0) {

        if( $depth != 0 ) {
            $opts['menu_item_heading_style'] = 'no';
        }

        return $opts;
    }
}

if(! function_exists('BMM_add_megamenu_optons_lvl_0')) {
    /**
     * @since 1.0.0
     *
     * @param {array} $opts
     * @param {int} $depth
     *
     * @return {array}
     */
    function BMM_add_megamenu_optons_lvl_0($opts = array(), $depth = 0) {

        if($depth == 0) {
            $opts['megamenu'] = 'no';
            $opts['minwidth'] = 'no';
            $opts['megamenu_alignment'] = 'left';
            // $opts['megamenu_background_image'] = '';
            // $opts['megamenu_background_color'] = '#FFF';
            // $opts['megamenu_full_width'] = 'no';
            // $opts['megamenu_background_image'] = '';
        }

        return $opts;
    }
}

if(! function_exists('BMM_add_megamenu_optons_lvl_1')) {
    /**
     * @since 1.0.0
     *
     * @param {array} $opts
     * @param {int} $depth
     *
     * @return {array}
     */
    function BMM_add_megamenu_optons_lvl_1($opts = array(), $depth = 0) {

        if($depth == 1) {
            $opts['custom_menu_item_width'] = 'no';
            $opts['menu_item_width'] = '300px';
        }

        return $opts;
    }
}

if(! function_exists('BMM_Get_Menu_Item_Data')) {
    /**
     * @since 1.0.0
     *
     * @param {int} $menuID
     * @param {int} $depth
     *
     * @return {array}
     */
    function BMM_Get_Menu_Item_Data($menuID = 0, $depth = 0) {
        $default_data = BMM_Default_Options($depth);
        $menu_item_data = get_post_meta($menuID, implode('_', array('_menu_item_menu_item_bmm', $depth)), true);

        if($menu_item_data) $menu_item_data_result = array_merge($default_data, $menu_item_data);
        else $menu_item_data_result = $default_data;

        return $menu_item_data_result;// array_merge($default_data, $menu_item_data || array());
    }
}

if(! function_exists('BMM_Load_Options_Menu_Item')) {
    /**
     * @since 1.0.0
     * load menu item options
     *
     * @param {array} $params
     *
     */
    function BMM_Load_Options_Menu_Item($params) {
        extract($params);
        $data = BMM_Get_Menu_Item_Data($menu_id, $menu_depth);
        wp_send_json_success( $data );
    }
}

if(! function_exists('BMM_Save_Menu_Item_Data')) {
    /**
     * @since 1.0.0
     */
    function BMM_Save_Menu_Item_Data($params) {
        extract($params);

        $menu_lvl = ($menu_depth == 0 || empty($menu_depth) || $menu_depth == false) ? '0' : $menu_depth;
        $menu_data = $data;
        $meta_key = implode('_', array('_menu_item_menu_item_bmm', $menu_lvl));

        $result = update_post_meta($menu_id, $meta_key, $menu_data);

        wp_send_json_success( $result );
    }
}

if(! function_exists('BMM_WP_Nav_Menu_Args_Filter')) {
    /**
     * @since 1.0.0
     * custom nav walker
     *
     * @param {array} $args
     * @return {array}
     */
    function BMM_WP_Nav_Menu_Args_Filter( $args ) {
        $args['menu_class'] = implode(' ', array( $args['menu_class'], 'bmm-megamenu-element' ));
        $args['walker'] = new BMM_Nav_Walker();
        return $args;
    }
}

if(! function_exists('BMM_Nav_Menu_Megamenu_Class_Func')) {
    /**
     * @since 1.0.0
     *
     */
    function BMM_Nav_Menu_Megamenu_Class_Func($classes_output, $item, $args, $depth, $bmm_menu_item_data) {

        // add class menu-item-heading-style
        if( isset($bmm_menu_item_data['custom_menu_item_width']) && $bmm_menu_item_data['custom_menu_item_width'] == 'yes' && !empty($bmm_menu_item_data['menu_item_width']) ) {
            $classes_output .= ' menu-item-has-custom-width';
        }

        // add class menu-item-heading-style
        if( isset( $bmm_menu_item_data['menu_item_heading_style'] ) && $bmm_menu_item_data['menu_item_heading_style'] == 'yes' ) {
            $classes_output .= ' menu-item-heading-style';
        }


        if( $depth != 0 ) return $classes_output;

        // add class menu-item-has-megamenu
        if( isset( $bmm_menu_item_data['minwidth'] ) && $bmm_menu_item_data['minwidth'] == 'yes' ) {
            $classes_output .= ' menu-item-has-subminwidth';
        }
        if( isset( $bmm_menu_item_data['megamenu'] ) && $bmm_menu_item_data['megamenu'] == 'yes' ) {
            $classes_output .= ' menu-item-has-megamenu';
        }

        return $classes_output;
    }
}

if(! function_exists('BMM_Before_Megamenu_Section_Func')) {
    /**
     * @since 1.0.0
     *
     */
    function BMM_Before_Megamenu_Section_Func($output, $item, $args, $depth, $bmm_menu_item_data) {
        if( $depth != 0 ) return;

        if( isset( $bmm_menu_item_data['megamenu'] ) && $bmm_menu_item_data['megamenu'] == 'yes' ) {
            $classes = array('bmm-megamenu-section');
            // megamenu_center_mode
            if( isset($bmm_menu_item_data['megamenu_alignment']) ){
                array_push($classes, 'bmm-megamenu-alignment-' . $bmm_menu_item_data['megamenu_alignment']);
            }
            return '<section class="'. implode(' ', $classes) .'">';
        }
    }
}

if(! function_exists('BMM_After_Megamenu_Section_Func')) {
    /**
     * @since 1.0.0
     *
     */
    function BMM_After_Megamenu_Section_Func($output, $item, $args, $depth, $bmm_menu_item_data) {
        if( $depth != 0 ) return;

        if( isset( $bmm_menu_item_data['megamenu'] ) && $bmm_menu_item_data['megamenu'] == 'yes' ) {
            return '</section>';
        }
    }
}

if(! function_exists('BMM_menu_item_title_add_icon')) {
    /**
     * @since 1.0.0
     */
    function BMM_menu_item_title_add_icon($output, $item, $args, $depth, $bmm_menu_item_data) {

        if( isset($bmm_menu_item_data['icon']) && $bmm_menu_item_data['icon'] == 'yes' && !empty($bmm_menu_item_data['icon_class']) ) {
            $output = '<span class="__bmm-menu-icon '. $bmm_menu_item_data['icon_class'] .'"></span>' . $output;
        }

        return $output;
    }
}

if(! function_exists('BMM_menu_item_title_add_badge')) {
    /**
     * @since 1.0.0
     */
    function BMM_menu_item_title_add_badge($output, $item, $args, $depth, $bmm_menu_item_data) {

        if( isset($bmm_menu_item_data['badge']) && $bmm_menu_item_data['badge'] == 'yes' && !empty($bmm_menu_item_data['badge_text']) ) {
            $output = $output . '<span class="__bmm-menu-badge" style="background: '.$bmm_menu_item_data['badge_background'].'; color: '.$bmm_menu_item_data['badge_color'].';">'. $bmm_menu_item_data['badge_text'] .'<i class="__bmm-menu-badge-arrow" style="color: '.$bmm_menu_item_data['badge_background'].';"></i></span>';
        }

        return $output;
    }
}

if(! function_exists('BMM_nav_menu_li_attributes_add_custom_width')) {
    /**
     * @since 1.0.0
     */
    function BMM_nav_menu_li_attributes_add_custom_width($li_atts = array(), $item, $args, $depth, $bmm_menu_item_data) {

        if( isset($bmm_menu_item_data['custom_menu_item_width']) && $bmm_menu_item_data['custom_menu_item_width'] == 'yes' && !empty($bmm_menu_item_data['menu_item_width']) ) {
            $li_atts['style'] = implode('; ', array(
                'min-width: ' . $bmm_menu_item_data['menu_item_width'],
                'width: ' . $bmm_menu_item_data['menu_item_width'],
                'max-width: ' . $bmm_menu_item_data['menu_item_width'],
            ));
        }

        return $li_atts;
    }
}
