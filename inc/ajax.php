<?php 

if(! function_exists('BMM_Ajax_Handle')) {
    /**
     * @since 1.0.0 
     */
    function BMM_Ajax_Handle() {
        extract($_POST);
        if($handle && function_exists($handle)) call_user_func($handle, $params);
        exist();
    }
    add_action( 'wp_ajax_BMM_Ajax_Handle', 'BMM_Ajax_Handle' );
    add_action( 'wp_ajax_nopriv_BMM_Ajax_Handle', 'BMM_Ajax_Handle' );
}

