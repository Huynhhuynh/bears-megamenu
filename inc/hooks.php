<?php 
/**
 * @package Bears Megamenu
 * @since 1.0.0
 * hooks
 */

add_action('admin_footer', 'BMM_Template');
add_filter( 'BMM_options_default_filter', 'BMM_add_megamenu_optons_lvl_0', 20, 2 );
add_filter( 'BMM_options_default_filter', 'BMM_add_megamenu_optons_lvl_1', 20, 2 );
add_filter( 'BMM_options_default_filter', 'BMM_add_megamenu_optons_add_opt_heading_style', 20, 2 );
add_filter( 'wp_nav_menu_args', 'BMM_WP_Nav_Menu_Args_Filter' );

add_filter( 'BMM_before_megamenu_section', 'BMM_Before_Megamenu_Section_Func', 20, 5 );
add_filter( 'BMM_after_megamenu_section', 'BMM_After_Megamenu_Section_Func', 20, 5 );
add_filter( 'BMM_nav_menu_megamenu_class', 'BMM_Nav_Menu_Megamenu_Class_Func', 20, 5 );
add_filter( 'BMM_menu_item_title', 'BMM_menu_item_title_add_icon', 20, 5 );
add_filter( 'BMM_menu_item_title', 'BMM_menu_item_title_add_badge', 20, 5 );
add_filter( 'nav_menu_li_attributes', 'BMM_nav_menu_li_attributes_add_custom_width', 20, 5 );