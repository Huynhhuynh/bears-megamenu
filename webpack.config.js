module.exports = {
    context: __dirname + '/assets/js',
    devtool: 'source-map',
    mode: 'development',
    entry: {
      'bears-megamenu': __dirname + '/assets/js/bears-megamenu.js',
      'backend-bears-megamenu': __dirname + '/assets/js/backend.bears-megamenu.js',
    },
    output: {
      path: __dirname + '/assets/js',
      filename: '[name].bundle.js',
    },
    resolve: {
      alias: {
        'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
      }
    }
  }
  