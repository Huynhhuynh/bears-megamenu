<?php
/*
 * Plugin Name: Bears Megamenu
 * Plugin URI: http://bearsthemes/
 * Description: Create Mega Menus create by Bearsthemes
 * Version: 1.0.0
 * Author: Bearsthemes
 * Author URI: http://bearsthemes.com/
 * Text Domain: bears-megamenu
 */

/**
 * Composer autoload & functions.php
 */
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/inc/ajax.php';
require __DIR__ . '/inc/functions.php';
require __DIR__ . '/inc/hooks.php';
require __DIR__ . '/inc/class-bmm-walker-nav-menu.php';

if( ! function_exists('bmm_plugin_links') ) {
    /**
     * Plugin page links
     * @since 1.0.0
     */
    function bmm_plugin_links( $links ) {

        $plugin_links = array(
            '<a href="#">' . __( 'Support', 'bears-megamenu' ) . '</a>',
            '<a href="#">' . __( 'Docs', 'bears-megamenu' ) . '</a>',
        );

        return array_merge( $plugin_links, $links );
    }

    add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'bmm_plugin_links' );
}

if( ! class_exists('Bears_Megamenu') ) {

    /**
     * Bears Megamenu - Main class
     * @since 1.0.0
     */
    class Bears_Megamenu {

        private $dev_mode = true;

        /**
         * @since 1.0.0
         */
        function __construct() {
            $this->defined();
            $this->hooks();
        }

        /**
         * @since 1.0.0
         */
        public function defined() {
            define( "BMM_MODE"       , $this->dev_mode );
            define( "BMM_DIR_URL"    , plugin_dir_url( __FILE__ ) );
            define( "BMM_DIR_PATH"   , plugin_dir_path( __FILE__ ) );
            define( "BMM_VERSION"    , '1.0.0' );
        }

        /**
         * @since 1.0.0
         */
        public function hooks() {
            // load script
            add_action( 'wp_enqueue_scripts', array($this, 'scripts') );
            add_action( 'admin_enqueue_scripts', array($this, 'admin_scripts') );
        }

        public function admin_scripts() {
            /* render css */
            BMM_Render_Scss( BMM_DIR_PATH . '/assets/css/backend.scss', BMM_DIR_PATH . '/assets/css/backend.bears-megamenu.css', BMM_DIR_PATH . '/assets/css' );

            wp_enqueue_script( 'vue', plugins_url( '/assets/vendor/vue/vue.min.js', __FILE__ ), array(), '2.4.4', true );
            wp_enqueue_script( 'vuex', plugins_url( '/assets/vendor/vue/vuex.min.js', __FILE__ ), array('vue'), '2.4.0', true );

            wp_enqueue_style( 'element-ui', plugins_url( '/assets/vendor/element-ui/element-ui.css', __FILE__ ), array(), '1.4.6', 'all' );
            wp_enqueue_script( 'element-ui', plugins_url( '/assets/vendor/element-ui/element-ui.js', __FILE__ ), array('vue'), '1.4.6', true );
            wp_enqueue_script( 'element-ui-en', plugins_url( '/assets/vendor/element-ui/element-ui-en.js', __FILE__ ), array('element-ui'), '1.4.6', true );
            // wp_add_inline_script( 'element-ui-en', 'ELEMENT.locale(ELEMENT.lang.en);' );

            /* script */
            wp_enqueue_style( 'backend-bears-megamenu', plugins_url( 'assets/css/backend.bears-megamenu.css', __FILE__ ), false, BMM_VERSION );
            wp_enqueue_script( 'backend-bears-megamenu', plugins_url( 'assets/js/backend-bears-megamenu.bundle.js', __FILE__ ), array('jquery', 'vue', 'vuex', 'element-ui'), BMM_VERSION, true );
        
            /* admin localize script( */
            wp_localize_script('backend-bears-megamenu', 'bmm_admin_localize_script', apply_filters( 'bmm_admin_localize_script', array(
                'ajax_url' => admin_url( 'admin-ajax.php' ),
                'menu_depth_support' => apply_filters( 'BMM_menu_depth_support', array(0, 1) ),
                'language' => array(
                    'TEXT_ADVANCED_SETTINGS' => __('Advanced Settings', 'bears-megamenu'),
                )
            ) ));
        }

        /**
         * scripts.
         * @since 1.0.0
         */
        public function scripts() {
            /* render css */
            BMM_Render_Scss( BMM_DIR_PATH . '/assets/css/main.scss', BMM_DIR_PATH . '/assets/css/bears-megamenu.css', BMM_DIR_PATH . '/assets/css' );

            /* script */
            wp_enqueue_style( 'bears-megamenu', plugins_url( 'assets/css/bears-megamenu.css', __FILE__ ), false, BMM_VERSION );
            wp_enqueue_script( 'bears-megamenu', plugins_url( 'assets/js/bears-megamenu.bundle.js', __FILE__ ), array('jquery'), BMM_VERSION, true );
            
            /* localize script( */
            wp_localize_script('bears-megamenu', 'bmm_localize_script', apply_filters( 'bmm_localize_script', array(
                'ajax_url' => admin_url( 'admin-ajax.php' ),
            ) ));
        }

    }

    $GLOBALS['Bears_Megamenu'] = new Bears_Megamenu();
}
